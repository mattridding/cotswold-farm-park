<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_cotswold' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*f|IY_!TI-vv=$=To%W*qK_(Wts)IlzN=F76QlLq=(iAthx 1?0X-!sA+p*%Be4M' );
define( 'SECURE_AUTH_KEY',  'M2WnEdpg?fV@p@)W@=@c_GKQ)z6h2PN kjB+XVp:F_JU~1];e$:$)^]Pc[vj%u9X' );
define( 'LOGGED_IN_KEY',    'b)L3tVYQJykr9m7U!@`?>,Q:&Q|H}hLuO1Y72^(B85p:el)wtN!xdQz9,ouYc@Hz' );
define( 'NONCE_KEY',        'eJWmhbdc;XReCA;zgyk~X[|st54~~(Ft!AUgt Wx&D4pFul6hj@tWB>p5CrXn>em' );
define( 'AUTH_SALT',        'A.(Uq%<H8|9yt#Uptf{yb,*KcA#!ObuNJ90,Vmy2x(I] 02QJbiqQu?S%>,Jp5qN' );
define( 'SECURE_AUTH_SALT', 'CdDNTXpj.1oY/`eqN)P?gj5t]1=(Doi]dKA,$OHa+[[,ZDUH5n@D~XHJ~T5 !Yw)' );
define( 'LOGGED_IN_SALT',   'W# WyT<P&i*v;yMcA/$(*pjmrxZp}~u#-Fsfw:&@@w:bstOSA!x*;%#ScKpGsUQ`' );
define( 'NONCE_SALT',       'v@|jfTvp&Lr+Cnf?XP.(kl}Z+gbYJ@)V@($Z; u$nC&F^wGI?>0T8Uzlno5s;Ya%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
