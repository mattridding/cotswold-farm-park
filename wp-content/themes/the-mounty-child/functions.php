<?php
/**
 * Child-Theme functions and definitions
 */

function themounty_child_scripts() {
    wp_enqueue_style( 'themounty-style', get_template_directory_uri(). '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'themounty_child_scripts' );

function register_my_menus() {
    register_nav_menus(
        array(
            'secondary-menu' => __( 'Secondary Menu' )
            // 'farm-park-secondary-menu' => __( 'Farm Park Secondary Menu' ),
            // 'stay-secondary-menu' => __( 'Stay Secondary Menu' ),
            // 'fun-rides-secondary-menu' => __( 'Fun Rides Secondary Menu' ),
            // 'adam-secondary-menu' => __( 'Adam Secondary Menu' ),
            // 'farming-secondary-menu' => __( 'Farming Secondary Menu' ),
            // 'contact-us-secondary-menu' => __( 'Contact us Secondary Menu' )
        )
    );
}