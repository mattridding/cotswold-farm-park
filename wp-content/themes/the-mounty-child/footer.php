<?php
/**
 * The Footer: widgets area, logo, footer menu and socials
 *
 * @package WordPress
 * @subpackage THE_MOUNTY
 * @since THE_MOUNTY 1.0
 */

						// Widgets area inside page content
						the_mounty_create_widgets_area('widgets_below_content');
						?>
					</div><!-- </.content> -->

					<?php
					// Show main sidebar
					get_sidebar();

					// Widgets area below page content
					the_mounty_create_widgets_area('widgets_below_page');

					$the_mounty_body_style = the_mounty_get_theme_option('body_style');
					if ($the_mounty_body_style != 'fullscreen') {
						?></div><!-- </.content_wrap> --><?php
					}
					?>
			</div><!-- </.page_content_wrap> -->

			<?php
			// Footer
			$the_mounty_footer_type = the_mounty_get_theme_option("footer_type");
			if ($the_mounty_footer_type == 'custom' && !the_mounty_is_layouts_available())
				$the_mounty_footer_type = 'default';
			get_template_part( "templates/footer-{$the_mounty_footer_type}");
			?>

		</div><!-- /.page_wrap -->

	</div><!-- /.body_wrap -->

	<?php if (false && the_mounty_is_on(the_mounty_get_theme_option('debug_mode')) && the_mounty_get_file_dir('images/makeup.jpg')!='') { ?>
		<img src="<?php echo esc_url(the_mounty_get_file_url('images/makeup.jpg')); ?>" id="makeup">
	<?php } ?>

	<?php wp_footer(); ?>

<!-- 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<script>
		if( !window.jQuery ) document.write('<script src="js/jquery-3.0.0.min.js"><\/script>');
	</script> -->

<script>
	jQuery(document).ready(function($){
		var mainHeader = $('.cd-auto-hide-header'),
			secondaryNavigation = $('.cd-secondary-nav'),
			//this applies only if secondary nav is below intro section
			belowNavHeroContent = $('.sub-nav-hero'),
			headerHeight = mainHeader.height();

		//set scrolling variables
		var scrolling = false,
			previousTop = 0,
			currentTop = 0,
			scrollDelta = 10,
			scrollOffset = 150;

		mainHeader.on('click', '.nav-trigger', function(event){
			// open primary navigation on mobile
			event.preventDefault();
			mainHeader.toggleClass('nav-open');
		});

		$(window).on('scroll', function(){
			if( !scrolling ) {
				scrolling = true;
				(!window.requestAnimationFrame)
					? setTimeout(autoHideHeader, 250)
					: requestAnimationFrame(autoHideHeader);
			}
		});

		$(window).on('resize', function(){
			headerHeight = mainHeader.height();
		});

		function autoHideHeader() {
			var currentTop = $(window).scrollTop();

			( belowNavHeroContent.length > 0 )
				? checkStickyNavigation(currentTop) // secondary navigation below intro
				: checkSimpleNavigation(currentTop);

		   	previousTop = currentTop;
			scrolling = false;
		}

		function checkSimpleNavigation(currentTop) {
			//there's no secondary nav or secondary nav is below primary nav
		    if (previousTop - currentTop > scrollDelta) {
		    	//if scrolling up...
		    	mainHeader.removeClass('is-hidden');
		    } else if( currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
		    	//if scrolling down...
		    	mainHeader.addClass('is-hidden');
		    }
		}

		function checkStickyNavigation(currentTop) {
			//secondary nav below intro section - sticky secondary nav
			var secondaryNavOffsetTop = belowNavHeroContent.offset().top - secondaryNavigation.height() - mainHeader.height();

			if (previousTop >= currentTop ) {
		    	//if scrolling up...
		    	if( currentTop < secondaryNavOffsetTop ) {
		    		//secondary nav is not fixed
		    		mainHeader.removeClass('is-hidden');
		    		secondaryNavigation.removeClass('fixed slide-up');
		    		belowNavHeroContent.removeClass('secondary-nav-fixed');
		    	} else if( previousTop - currentTop > scrollDelta ) {
		    		//secondary nav is fixed
		    		mainHeader.removeClass('is-hidden');
		    		secondaryNavigation.removeClass('slide-up').addClass('fixed');
		    		belowNavHeroContent.addClass('secondary-nav-fixed');
		    	}

		    } else {
		    	//if scrolling down...
		 	  	if( currentTop > secondaryNavOffsetTop + scrollOffset ) {
		 	  		//hide primary nav
		    		mainHeader.addClass('is-hidden');
		    		secondaryNavigation.addClass('fixed slide-up');
		    		belowNavHeroContent.addClass('secondary-nav-fixed');
		    	} else if( currentTop > secondaryNavOffsetTop ) {
		    		//once the secondary nav is fixed, do not hide primary nav if you haven't scrolled more than scrollOffset
		    		mainHeader.removeClass('is-hidden');
		    		secondaryNavigation.addClass('fixed').removeClass('slide-up');
		    		belowNavHeroContent.addClass('secondary-nav-fixed');
		    	}

		    }
		}
	});
</script>

</body>
</html>