<?php
/*
Plugin Name: Review Widgets for Tripadvisor
Plugin Title: Review Widgets for Tripadvisor Plugin
Plugin URI: https://wordpress.org/plugins/review-widgets-for-tripadvisor/
Description: Tripadvisor review widgets. Show your Tripadvisor reviews on your WordPress website to build trust and increase your SEO.
Tags: tripadvisor, tripadvisor reviews, tripadvisor recommendations, tripadvisor widgets, reviews
Author: Trustindex.io <support@trustindex.io>
Author URI: https://www.trustindex.io/
Contributors: trustindex
License: GPLv2 or later
Version: 1.5.3
Text Domain: trustindex
Domain Path: /languages/
Donate link: https://www.trustindex.io/pro-package/
*/
/*
Copyright 2019 Trustindex Kft (email: support@trustindex.io)
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require(ABSPATH . 'wp-includes/version.php');

/*****************************************************************************/
/* LOAD COMMON CODE */
if (!class_exists('TrustindexPlugin' ) )
{
	require_once($plugin_dir . "trustindex-plugin.class.php");
}
$trustindex_pm_tripadvisor = new TrustindexPlugin("tripadvisor", __FILE__, "1.5.3", "Review Widgets for Tripadvisor");

/*****************************************************************************/
/* INIT HOOKS */
//activation hook
register_activation_hook(__FILE__, array($trustindex_pm_tripadvisor, 'activate'));

//deactivation hook
register_deactivation_hook(__FILE__, array($trustindex_pm_tripadvisor, 'deactivate'));

//load i18n
add_action('plugins_loaded', array($trustindex_pm_tripadvisor, 'loadI18N'));

/******************************************************************************
/* ADMIN MENU */
// menu
add_action('admin_menu', array($trustindex_pm_tripadvisor, 'add_setting_menu'), 10);

//plugin list menu
add_filter('plugin_action_links', array($trustindex_pm_tripadvisor, 'add_plugin_action_links'), 10, 2);

//plugin page meta links.
add_filter( 'plugin_row_meta', array($trustindex_pm_tripadvisor, 'add_plugin_meta_links'), 10, 2 );

/*****************************************************************************/
/* WIDGET */
// widget init
add_action('widgets_init', array($trustindex_pm_tripadvisor, 'init_widget'));

//widget register
add_action('widgets_init', array($trustindex_pm_tripadvisor, 'register_widget'));

/*****************************************************************************/
/* SHORTCODE */
//init
add_action( 'init', array($trustindex_pm_tripadvisor, 'init_shortcode'));

/*****************************************************************************/
/* TINYMCE BUTTON */
//register plugin to tinyMCE
add_action( 'init', array($trustindex_pm_tripadvisor, 'register_tinymce_features') );

/*****************************************************************************/
/* APP OUTPUT BUFFER */
add_action( 'init', array($trustindex_pm_tripadvisor, 'output_buffer') );

/*****************************************************************************/
/* AJAX FUNCTIONS */
add_action( 'wp_ajax_list_trustindex_widgets', array($trustindex_pm_tripadvisor, 'list_trustindex_widgets_ajax') );
add_action( 'admin_enqueue_scripts', array($trustindex_pm_tripadvisor, 'trustindex_add_scripts') );

?>