=== Review Widgets for Tripadvisor ===
Contributors: trustindex
Donate link: https://www.trustindex.io/pro-package/
Tags: tripadvisor, tripadvisor reviews, tripadvisor recommendations, tripadvisor widgets, reviews
Requires at least: 3.0.1
Tested up to: 5.3
Requires PHP: 5.2
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Plugin Name: Review Widgets for Tripadvisor
Plugin Title: Review Widgets for Tripadvisor Plugin
Plugin URI: https://wordpress.org/plugins/review-widgets-for-tripadvisor/
Author: Trustindex.io <support@trustindex.io>
Author URI: https://www.trustindex.io/
Text Domain:  trustindex
Version: 1.5.3

Tripadvisor review widgets. Show your Tripadvisor reviews on your WordPress website to build trust and increase your SEO.

== Description ==

Publish your **Tripadvisor reviews** and **recommendations** with our **FREE WIDGETS** on your Wordpress website.

Get more **customer feedback** and **testimonials** easily to make customers **trust your business!**

### Would you like to increase your sales and conversions on your WordPress website?

 In case of WordPress websites, our **Tripadvisor reviews** and **recommendations** widget tool is excellent to build customer trust.

### Why are Tripadvisor reviews and recommendations so important in creating customer trust?

Newly-established companies often complain that their online campaigns are unsuccessful, they fail to effectively advertise on Tripadvisor or that their Google Ads campaigns don't bring the expected results.

Thanks to the countless effective examples, we are perfectly aware of the fact that **if your visitors trust your company better**, it will result in more customers and significantly higher conversion rates without investing a lot more in marketing.

### How to make customers trust your business with Tripadvisor review and recommendation widgets?

In case of a WordPress page, trust has several important components: quality of content displayed on the website, website design, synergy and relevance between the campaign and landing pages or the available payment options. You can find a lot of useful tools for these, therefore we don't wish to deal with these now.

However, there is one component that rarely comes to mind despite the fact that in practice it is one of **the most important factors when making a purchase decision**.

In our experience having the necessary number (150-200 pieces) of **quality Tripadvisor review and recommendation** may result in 70% more customers or higher conversion rates without spending more on marketing.

Tripadvisor is the most popular website in the world known by everyone and the ** credibility of a review can be immediately checked thanks to the profiles**; this explains why customer ratings available on Tripadvisor are so important.

### How can you get real customer reviews on Tripadvisor with our help?

1. **The plugin can be installed in an easy and fast way**. Setting the plugin up **does not require developer skills**.
1. All you need to do is to **display your filtered, relevant Tripadvisor reviews and recommendation** on your different sales pages.

### How can you display your real Tripadvisor recommendations on your sales pages?

* You have the chance to label the requested Tripadvisor recommendations and to display filtered reviews on your sales pages.
* Every Tripadvisor review can be tagged, you have the chance to filter by tag name.
* In the Tripadvisor widget used on your website, you have the option to show, only your best reviews. In other words, you can filter your Tripadvisor reviews by the number of stars.

### Why do Tripadvisor reviews increase the revenue in such an effective way?

Thanks to continuous, numerous and relevant Tripadvisor recommendations, customer trust in your online brand will start to increase.

At the same time you can be sure that you will experience a boosting conversion rate, therefore the willingness to purchase as well as the average order value will rise.

Thanks to the high conversion rates, more and more campaigns and advertisement channels that used to be suffering losses will become affordable for you.

### How can I monitor the processes?

With the help of our statistics, you can get to know how many additional real reviews you can obtain in a month through the system of automatic invitations.

### PLUGIN FEATURES

* Use our powerful Tripadvisor recommendation widgets
* Integrate your Tripadvisor reviews into your sidebar
* Generate shortcodes and integrate your Tripadvisor recommendations into your sales pages
* Display your filtered, relevant Tripadvisor ratings
* Hide your negative Tripadvisor reviews on sales pages
* Tag easily your Tripadvisor recommendations with our tag management system!
* Integrate your Tripadvisor reviews into your Email signatures!

[MORE FEATURES ON TRUSTINDEX.IO. USE IT FOR FREE!](https://www.trustindex.io/ "Trustindex.io")

### IMPORTANT

Tripadvisor reviews alone are ineffective or could even be harmful in case of enterprises that pursue bad business model, offer poor quality services, sell bad products or follow a bad sales strategy.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Trustindex.io screen to configure the plugin.
4. You can activate our Wordpress plugin after registering a FREE Account!

== Frequently Asked Questions ==

= How does Trustindex work? =

With Trustindex you can easily collect customer feedback on Tripadvisor, and publish these reviews in your sale system. The aim of Trustindex is to build customer trust, thus boost conversions and sales.

= How simple is it to install Trustindex? =

You do not need any developer skills to install and use the plugin.

= How long does it take to install Trustindex? =

The whole installation process takes around 3-4 minutes. It is worth to read the installation manual on admin page, as it contains all the detailed information and a step by step guide.

= What happens to my Tripadvisor reviews and recommendations if I stop using Trustindex? =

The reviews will remain on your Tripadvisor page.

= What should I do if I need any help? =

If you have any questions or problems, please send an email to support@trustindex.io and our colleagues will answer you in 48 hours!

= What do I need to use the FREE Trustindex package? =

Please verify that you have admin access to your Tripadvisor business page, as when installing Trustindex, you will have to sign in to your Tripadvisor account, and allow its usage.

= Do I have to pay when registering to Trustindex? =

You do not have to pay when you register, you neither have to provide your credit card details. The free account offers unlimited access to our tools.

== Screenshots ==

1. Tripadvisor reviews widget screenshot
2. Tripadvisor reviews badget screenshot
3. Tripadvisor reviews badget screenshot
3. Wordpress admin screenshot

== Changelog ==

= 1.5 =
Bugfixed!

= 1.4 =
Setup guide divided into 3 sections for faster Tripadvisor and Trustindex setup.

= 1.3 =
* Simplified setup to integrate Tripadvisor reviews easily.
* Added Trustindex prices tab
* Minor changes to meet Wordpress requirements.

= 1.2 =
You can use this plugin without any registration. Easier review integratation: no need to tripadvisor API, technical skills and so on...

= 1.1 =
Some security issues solved.

= 1.0 =
Integrate your Tripadvisor reviews into your website

== Upgrade Notice ==

= 1.5 =
Please upgrade!

= 1.1 =
Enjoy our plugin, you have nothing to check!

= 1.0 =
Make a backup!

== Support ==
Email: support@trustindex.io